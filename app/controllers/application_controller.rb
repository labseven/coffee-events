class ApplicationController < ActionController::Base
  before_action :require_login

  protect_from_forgery unless: -> { request.format.json? }

  private
    def require_login
      authenticate_or_request_with_http_basic do |name, password|
        password == ENV['ADMIN_PASS']
      end
    end
end
