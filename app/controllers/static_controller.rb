class StaticController < ApplicationController
  def index
    @num_subscribers = Subscriber.count
    @events = Event.where("start_date > ?", Date.today.beginning_of_day).order("start_date ASC")
  end

  def style
  end
end
