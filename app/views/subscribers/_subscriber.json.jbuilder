json.extract! subscriber, :id, :endpoint, :user_agent, :ip_addr, :name, :friend, :created_at, :updated_at
json.url subscriber_url(subscriber, format: :json)
