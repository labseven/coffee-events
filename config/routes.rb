Rails.application.routes.draw do
  root :to => 'static#index'

  resources :notifications
  post '/subscribers/unsubscribe', to: 'subscribers#destroy_by_subscription'
  resources :subscribers
  get '/events/upcoming', to: 'events#upcoming'
  resources :events
end
