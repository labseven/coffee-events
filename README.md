# Coffee Events Server

The backend for the [Coffee Events PWA](https://gitlab.com/labseven/coffee-events-pwa) that sends notifications about upcoming coffee events.


TODO:
* ~~Build frontend~~
* ~~Make Time input in local timezone~~
* ~~Allow Notification creation~~
* ~~Have new Notification send push notification to Subscribers~~
* Host VAPID public key
* Log subscriber PII to be able to sell data to advertisers (And get user statistics)
* ~~Add error handeling on notifications NotRegistered~~
* ~~Make root admin page~~


A preview of the admin interface. Simple and usable.
![admin interface](https://gitlab.com/labseven/coffee-events/raw/master/admin_preview.png)
