class RenameTimeStartInEvent < ActiveRecord::Migration[5.2]
  def change
    rename_column :events, :time_start, :start_date
  end
end
