class CreateSubscribers < ActiveRecord::Migration[5.2]
  def change
    create_table :subscribers do |t|
      t.json :subscription
      t.string :user_agent
      t.string :ip_addr
      t.string :name
      t.boolean :friend

      t.timestamps
    end
  end
end
