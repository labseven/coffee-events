class AddSubscriptionDetailsToSubscribers < ActiveRecord::Migration[5.2]
  def change
    add_column :subscribers, :endpoint, :string
    add_column :subscribers, :p256dh, :string
    add_column :subscribers, :auth, :string
    remove_column :subscribers, :subscription
  end
end
